//
//  EAProductVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit
import IlhasoftCore
import RAMReel
import CollieGallery
import MBProgressHUD

class EAProductVC: GAITrackedViewController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btShare: EAButton!
    @IBOutlet var btExit: UIButton!
    
    fileprivate var pricesPerCNPJ = [String: Double]()
    fileprivate var product: EAProduct!
    fileprivate var locations: [EALocation] = [EALocation]()
    //MARK: Init
    init(_ product: EAProduct) {
        super.init(nibName: "EAProductVC", bundle: nil)
        self.product = product
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = EAScreens.product.rawValue
        setupCollectionView()
        btShare.animationIntensity = .medium
        btExit.roundCorners(btExit.frame.width/2)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadLocations()
    }
    
    fileprivate func setupCollectionView() {
        collectionView.register(UINib(nibName: EALocationInfoCVC.reuseId, bundle: Bundle(for: EALocationInfoCVC.self)), forCellWithReuseIdentifier: EALocationInfoCVC.reuseId)
        collectionView.register(UINib(nibName: EAProductInfoCVC.reuseId, bundle: Bundle(for: EAProductInfoCVC.self)), forCellWithReuseIdentifier: EAProductInfoCVC.reuseId)
        collectionView.register(UINib(nibName: EAProductHeaderView.reuseId, bundle: Bundle(for: EAProductHeaderView.self)), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: EAProductHeaderView.reuseId)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    fileprivate func loadLocations() {
        MBProgressHUD.showAdded(to: view, animated: true)
        EASefazAPI.shared.getLocationsSelling(product) {
            (locations, pricesPerCNPJ) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.locations = locations ?? []
            self.pricesPerCNPJ = pricesPerCNPJ ?? [String: Double]()
            self.collectionView.reloadData()
            
            if locations == nil {
                ISAlertMessages.displayMessage("Houve um erro na conexão. Tentamos jajá?", withTitle: "Ops...", fromController: self)
            }
        }
    }
    
    //MARK: Touch
    @IBAction func btShareDidTap(_ sender: Any) {
        EANavigation.shared.showShareSheet(collectionView, inViewController: self)
    }
    
    @IBAction func btExitDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension EAProductVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return locations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EALocationInfoCVC.reuseId, for: indexPath) as! EALocationInfoCVC
        let cnpj = locations[indexPath.row].cnpj
        let price = cnpj == nil ? nil : pricesPerCNPJ[cnpj!]
        cell.setup(with: locations[indexPath.row], price: price)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 265)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: EAProductHeaderView.reuseId, for: indexPath) as! EAProductHeaderView
        view.setup(with: product)
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let location = (collectionView.cellForItem(at: indexPath) as! EALocationInfoCVC).location {
            EANavigation.shared.showLocationScreen(location, inNavigationController: navigationController)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width-50, height: 177)
    }
}
