//
//  EAProductHeaderView.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 13/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

class EAProductHeaderView: UICollectionReusableView {
    static let reuseId = "EAProductHeaderView"
    @IBOutlet var lbAverage:UILabel!
    @IBOutlet var lbBestPrice:UILabel!
    @IBOutlet var lbWorstPrice:UILabel!
    @IBOutlet var lbName: UILabel!
    
    
    func setup(with product: EAProduct) {
        EASefazAPI.shared.getProducts(byBarCode: product.barCode, coordinates: EALocation.available) { (response) in
            if let fullProduct = response?["product"] as? EAProduct {
                self.lbName.text = fullProduct.name?.localizedUppercase
                
                if let average = fullProduct.averagePrice.value as? Double {
                    self.lbAverage.text = "R$\(String(format: "%.2f", average))"
                } else {
                    self.lbAverage.text = "?"
                }
                
                if let bestPrice = fullProduct.bestPrice.value as? Double {
                    self.lbBestPrice.text = "R$\(String(format: "%.2f", bestPrice))"
                } else {
                    self.lbBestPrice.text = "?"
                }
                
                if let worstPrice = fullProduct.worstPrice.value as? Double {
                    self.lbWorstPrice.text = "R$\(String(format: "%.2f", worstPrice))"
                } else {
                    self.lbWorstPrice.text = "?"
                }
            }
            
        }
        
    }

}
