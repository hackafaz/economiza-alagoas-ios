
//
//  EAMainListsCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

protocol EAMainCVCBase {
    func setup()
}

class EAMainListsCVC: UICollectionViewCell, EAMainCVCBase {
    static let reuseId: String = "EAMainListsCVC"
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lbName: UILabel!

    fileprivate var favoriteProducts = [EAProduct]()
    fileprivate var favoriteLocations = [EALocation]()
    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    
    fileprivate func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self

        collectionView.register(UINib(nibName: EAMoreItemsCVC.reuseId, bundle: Bundle(for: EAMoreItemsCVC.self)), forCellWithReuseIdentifier: EAMoreItemsCVC.reuseId)
        collectionView.register(UINib(nibName: EALocationInfoCVC.reuseId, bundle: Bundle(for: EALocationInfoCVC.self)), forCellWithReuseIdentifier: EALocationInfoCVC.reuseId)
        collectionView.register(UINib(nibName: EAProductInfoCVC.reuseId, bundle: Bundle(for: EAProductInfoCVC.self)), forCellWithReuseIdentifier: EAProductInfoCVC.reuseId)
        collectionView.register(UINib(nibName: EAListsHeaderView.reuseId, bundle: Bundle(for: EAListsHeaderView.self)), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: EAListsHeaderView.reuseId)
        favoriteProducts = EAUser.current().getProducts()
        favoriteLocations = EAUser.current().getLocations()
        collectionView.reloadData()
    }
    
    func setup() {
//        self.items = EAUser.current().favoriteProducts
    }
}

extension EAMainListsCVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var items: Int!
        if section == 0 {
            items = favoriteProducts.count + 1
        } else {
            items = favoriteLocations.count
        }
        return items
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: EAListsHeaderView.reuseId, for: indexPath) as! EAListsHeaderView
        let title = indexPath.section == 0 ? "PRODUTOS" : "LOCAIS"
        view.setup(with: title)
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: section == 0 ? 40 : 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        var size = CGSize.zero
        if section == 1 {
            size = CGSize(width: collectionView.frame.width, height: 40)
        }
        return size 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        if indexPath.section == 0 {
            let reuseId = indexPath.row == favoriteProducts.count ? EAMoreItemsCVC.reuseId : EAProductInfoCVC.reuseId
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath)
            if indexPath.row <= favoriteProducts.count-1 {
                (cell as? EAProductInfoCVC)?.setup(with: favoriteProducts[indexPath.row], price: nil, isBestPrice: true)
            }
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: EALocationInfoCVC.reuseId, for: indexPath)
            (cell as! EALocationInfoCVC).setup(with: favoriteLocations[indexPath.row], price: nil)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == favoriteProducts.count {
                let nav = UINavigationController(rootViewController: EASearchVC())
                nav.EAAppearance()
                EANavigation.shared.nav.present(nav, animated: true)
            } else {
                let product = favoriteProducts[indexPath.row]
                EANavigation.shared.showProductScreen(product)
            }
        } else {
            let location = favoriteLocations[indexPath.row]
            EANavigation.shared.showLocationScreen(location)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize!
        if indexPath.section == 0 {
            size = CGSize(width: 150, height: 80)
        } else {
            size = CGSize(width: collectionView.frame.width, height: 177)
        }
        return size
    }
}
