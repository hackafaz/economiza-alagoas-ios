//
//  EAMainCategoriesFlowLayout.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import UIKit

class EAMainCategoriesFlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 10
        minimumLineSpacing = 10
        scrollDirection = .vertical
    
    }
    
//    override func layoutAttributesForElements(in rect: CGRect) ->     [UICollectionViewLayoutAttributes]? {
//        guard let oldAttributes = super.layoutAttributesForElements(in: rect) else {
//            return super.layoutAttributesForElements(in: rect)
//        }
//        let spacing = CGFloat(10) // REPLACE WITH WHAT SPACING YOU NEED
//        var newAttributes = [UICollectionViewLayoutAttributes]()
//        var leftMargin = self.sectionInset.left
//        for attributes in oldAttributes {
//            if (attributes.frame.origin.x == self.sectionInset.left) {
//                leftMargin = self.sectionInset.left
//            } else {
//                var newLeftAlignedFrame = attributes.frame
//                newLeftAlignedFrame.origin.x = leftMargin
//                attributes.frame = newLeftAlignedFrame
//            }
//            
//            leftMargin += attributes.frame.width + spacing
//            newAttributes.append(attributes)
//        }
//        return newAttributes
//    }
    
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
