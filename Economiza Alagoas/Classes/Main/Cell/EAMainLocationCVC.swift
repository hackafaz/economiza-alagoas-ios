//
//  EAMainLocationCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import IlhasoftCore

protocol EAMainLocationCVCDelegate {
    func enterSearch()
    func enterMap()
}

class EAMainLocationCVC: UICollectionViewCell {
    static let reuseId: String = "EAMainLocationCVC"
   
    @IBOutlet var btMap: EAButton!
    @IBOutlet var btCamera: EAButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbLocationTopConstraint: NSLayoutConstraint!
    
    var items = [Any]()
    var delegate: EAMainLocationCVCDelegate?
    
    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        searchView.roundCorners(7.5)
        searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapSearchView)))
        setupButtons()
    }
    
    fileprivate func setupButtons() {
        btCamera.roundCorners(btCamera.frame.width/2)
        btCamera.animationIntensity = .medium
        btCamera.ignoreStateChanges = true
        btMap.roundCorners(btMap.frame.width/2)
        btMap.animationIntensity = .medium
        btMap.ignoreStateChanges = true
    }
    
    func setup() {
//        self.items = EAUser.current().favoriteLocations
    }
    
    //MARK: Util
    
    
    //MARK: Touch
    @objc fileprivate func didTapSearchView() {
        delegate?.enterSearch()
    }
    
    @IBAction func btMapDidTouch(_ sender: Any) {
        delegate?.enterMap()
    }
    
    @IBAction func btCameraDidTouch(_ sender: Any) {
        let nav: UIViewController = UINavigationController(rootViewController: EASearchVC(goToCamera: true))
        let center = btCamera.convert(btCamera.center, to: contentView)
        EANavigation.shared.bubblePresent(nav, center: center, color: UIColor(rgba: "#4DD3E1"))
    }
}
