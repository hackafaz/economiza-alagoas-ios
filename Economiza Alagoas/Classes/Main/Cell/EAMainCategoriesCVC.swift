//
//  EAMainCategoriesCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

class EAMainCategoriesCVC: UICollectionViewCell, EAMainCVCBase {
    static let reuseId: String = "EAMainCategoriesCVC"
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lbName: UILabel!
    
    fileprivate var gridLayout = EAMainCategoriesFlowLayout()
    var items = [Any]()

    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    
    fileprivate func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: EACategoryCVC.reuseId, bundle: Bundle(for: EACategoryCVC.self)), forCellWithReuseIdentifier: EACategoryCVC.reuseId)
        collectionView.setCollectionViewLayout(gridLayout, animated: false)
        collectionView.reloadData()
    }
    
    func setup() {
//        self.items = EAUser.current().favoriteProducts
    }
}

extension EAMainCategoriesCVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5 //EAUser.current().favoriteCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EACategoryCVC.reuseId, for: indexPath) as! EACategoryCVC
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UINavigationController(rootViewController: EASearchVC())
        vc.EAAppearance()
        EANavigation.shared.nav.present(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let availableWidths: [CGFloat] = [100, 120, 140]
//        let width = availableWidths.sample()
        return CGSize(width: (collectionView.frame.width/2)-10, height: 100)
    }
}
