//
//  EAMainVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit
import IlhasoftCore
import BubbleTransition

class EAMainVC: UIViewController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var indicator: EALocationIndicatorView!
    @IBOutlet var mapView: EAMapView!
    @IBOutlet var exitMapButton: UIButton!
    @IBOutlet var indicatorTopConstraint: NSLayoutConstraint!
    fileprivate let visibleSearchTopConstraints: (cellLabel: CGFloat, indicator: CGFloat) = (cellLabel: -90, indicator: -100)
    fileprivate let hiddenSearchTopConstraints: (cellLabel: CGFloat, indicator: CGFloat) = (cellLabel: 45, indicator: 85)
    fileprivate var isShowingMap = false
    
    //MAP
    fileprivate var selectedIndexPath: IndexPath!
    
    //TODO: provisory
    let screens: [EAScreens] = [.localsTab, .listsTab, .categoriesTab]
    let identifiers = [EAMainLocationCVC.reuseId, EAMainListsCVC.reuseId]
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupNavigatorIndicatorView()
        mapView.setup(with: EAUser.current().getLocations(), pricesPerCNPJ: nil, delegate: self, showGallery: false, openLocationOnTap: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        EAAnalytics.shared.track(screens[selectedIndexPath.row])
    }
    
    fileprivate func setupCollectionView() {
        exitMapButton.alpha = 0
        collectionView.dataSource = self
        collectionView.delegate = self
        for id in identifiers {
            collectionView.register(UINib(nibName: id, bundle: Bundle(for: EAMainLocationCVC.self)), forCellWithReuseIdentifier: id)
        }
        collectionView.reloadData()
        selectedIndexPath = IndexPath(row: 0, section: 0)
    }
    
    fileprivate func setupNavigatorIndicatorView() {
        indicator.setup(lineColor: UIColor(rgba: "#DBD9DB"), activeLineColor: UIColor(rgba: "#B098A4"), lineHeight: 2, sections: 2)
        indicator.setLineAlpha(to: 0)
        exitMapButton.roundCorners(exitMapButton.frame.width/2)
    }
    
    fileprivate func exitMap() {
        DispatchQueue.main.async {
            self.mapView.centerMapOnLocation(location: self.mapView.location)
            self.isShowingMap = false
            self.collectionView.isHidden = false

            let cell = self.collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as? EAMainLocationCVC
            self.collectionView.isHidden = false
            cell?.contentView.layoutIfNeeded()
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
                self.indicatorTopConstraint.constant = self.hiddenSearchTopConstraints.indicator
                self.exitMapButton.alpha = 0
                cell?.searchView.alpha = 1
                cell?.btCamera.alpha = 1
                cell?.btMap.alpha = 1
                cell?.lbLocationTopConstraint.constant = self.hiddenSearchTopConstraints.cellLabel
                cell?.contentView.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }) {
                success in
            }
        }
    }
    
    func enterMap() {
        DispatchQueue.main.async {
            if !self.isShowingMap {
                self.isShowingMap = true
                let cell = self.collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as? EAMainLocationCVC
                cell?.contentView.layoutIfNeeded()
                self.view.layoutIfNeeded()
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                    self.indicatorTopConstraint.constant = self.visibleSearchTopConstraints.indicator
                    cell?.btCamera.alpha = 0
                    cell?.btMap.alpha = 0
                    cell?.searchView.alpha = 0
                    cell?.lbLocationTopConstraint.constant = self.visibleSearchTopConstraints.cellLabel
                    self.exitMapButton.alpha = 1
                    cell?.contentView.layoutIfNeeded()
                    self.view.layoutIfNeeded()
                }) {
                    success in
                    self.collectionView.isHidden = true
                }
            }
        }
    }
    
    //MARK: Touch
    @IBAction func exitButtonDidTap(_ sender: UIButton) {
        exitMap()
    }
    
}

extension EAMainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            enterMap()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let id = identifiers[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath)
        (cell as? EAMainCVCBase)?.setup()
        
        if id == EAMainLocationCVC.reuseId {
            (cell as? EAMainLocationCVC)?.delegate = self
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    //MARK: Scroll delegate methods
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indicator.hideTrailLine(false)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let fraction = collectionView.contentOffset.x/collectionView.frame.width
        indicator.setLineAlpha(to: fraction)
        indicator.moveActiveLine(fraction: fraction)
        collectionView.backgroundColor = UIColor(colorLiteralRed: 254/255, green: 252/255, blue: 251/255, alpha: Float(fraction))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var row = Int(floor(collectionView.contentOffset.x/collectionView.frame.width))
        row = row < 0 ? 0 : row
        row = row > 1 ? 1 : 1
        selectedIndexPath = IndexPath(row: row, section: 0)
        
        EAAnalytics.shared.track(screens[selectedIndexPath.row])
        indicator.hideTrailLine(true)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    
    }
}

extension EAMainVC: EAMainLocationCVCDelegate {
    func enterSearch() {
        let nav = UINavigationController(rootViewController: EASearchVC())
        nav.EAAppearance()
        let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as! EAMainLocationCVC
        EANavigation.shared.bubblePresent((nav as UIViewController), center: cell.searchView.center, color: .white)
    }
}

extension EAMainVC: EAMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var view: EAAnnotationView?
        if let annotation = annotation as? EAMapAnnotation {
            let identifier = annotation.type
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier.rawValue) as? EAAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = EAAnnotationView(annotation: annotation, reuseIdentifier: identifier.rawValue, location: annotation.location)
            }
        }
        return view
    }
    
    func getParentNavigationController() -> UINavigationController? {
        return nil
    }
    
    func didUpdateLocation(to location: CLLocation) {
    }
    
    fileprivate func showVacancies() {
//        mapView.removeAnnotations(locationAnnotations)
//        locationAnnotations = self.locations.map ({
//            (location: EALocation) -> EAMapAnnotation in
//            return EAMapAnnotation(location: location, type: .regular)
//        })
//        mapView.addAnnotations(locationAnnotations)
    }
}
