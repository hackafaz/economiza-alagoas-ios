//
//  EAListsHeaderView.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 13/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

class EAListsHeaderView: UICollectionReusableView {
    static let reuseId = "EAListsHeaderView"
    
    @IBOutlet var lbTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setup(with title: String) {
       lbTitle.text = title
    }

}
