//
//  EAAnalytics.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 14/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation


class EAAnalytics {
    static let shared = EAAnalytics()
    private(set) public var tracker: GAITracker!
    
    var lastTrackedScreen: String?
    
    private init() {}
    
    func setTracker(to tracker: GAITracker) {
        self.tracker = tracker
    }
    
    func track(_ screen: EAScreens) {
        if screen.rawValue != EAAnalytics.shared.lastTrackedScreen {
            EAAnalytics.shared.lastTrackedScreen = screen.rawValue
            EAAnalytics.shared.tracker.set(kGAIScreenName, value: screen.rawValue)
            EAAnalytics.shared.tracker.send(GAIDictionaryBuilder.createScreenView()?.build() as [NSObject : AnyObject]!)
        }
    }
}


enum EAScreens: String {
    case localsTab = "Locals Tab"
    case listsTab = "Lists Tab"
    case categoriesTab = "Categories Tab"
    case map = "Map"
    case search = "Search"
    case product = "Product"
    case category = "Category"
    case local = "Local"
}
