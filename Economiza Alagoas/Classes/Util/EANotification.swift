//
//  EANotification.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 19/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation

enum EANotificationName: String {
    case newFavoriteLocation = "newFavoriteLocation"
    case newFavoriteProduct = "newFavoriteProduct"
}

class EANotification {
    
    static func newFavoriteLocation() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: EANotificationName.newFavoriteLocation.rawValue), object: self)
    }
    
    static func newFavoriteProduct() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: EANotificationName.newFavoriteProduct.rawValue), object: self)
    }
}
