//
//  EAPlacesManager.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import MapKit
import GooglePlaces
import GooglePlacePicker
import SCGoogleSearch


class EAPlacesInteractor {
    static let shared = EAPlacesInteractor()
    
    private init() { }
    
    func getPlaceInfo(query: String, location: CLLocationCoordinate2D, completion: @escaping ((_ place: GMSPlace?, _ error: Error?)->())) {
        //TODO:
        completion(nil, nil)
//        let image = SCGoogleImage()
//        image.title = ""
////        image.search
//        
//        let search = SCGoogleSearch(key: AppDelegate.gID, withCx: "")
//        search?.loadPictures(withName: "palato maceio al") {
//            (images, pagination, error) in
//            print(error?.localizedDescription)
//            if let images = images {
//                
//            }
//        }
        
//        let correctedAddress:String! = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress)&sensor=false")
//        
//        
//        
//        
//        _ = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
//            // 3
//            do {
//                if data != nil{
//                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as!  NSDictionary
//                    print(dic)
////                    let lat = ((dic["results"] as AnyObject).value(forKey: "geometry")? as AnyObject).valueForKey("location")?.valueForKey("lat")?.objectAtIndex(0) as! Double
////                    let lon = (dic["results"] as AnyObject).value(forKey: "geometry")?.valueForKey("location")?.valueForKey("lng")?.objectAtIndex(0) as! Double
//                    // 4
//                }
//            }catch {
//                print("Error")
//            }
//            
//        
////        let northEast = CLLocationCoordinate2D(latitude: location.latitude + 0.01, longitude: location.longitude + 0.01)
////        let southWest = CLLocationCoordinate2D(latitude: location.latitude - 0.01, longitude: location.longitude - 0.01)
////        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
////        
////        let filter = GMSAutocompleteFilter()
////        filter.type = .establishment
////        GMSPlacesClient.shared().autocompleteQuery(query, bounds: nil, filter: filter) {
////            (results, error) in
////            if let placeId = results?.first?.placeID {
////                GMSPlacesClient.shared().lookUpPlaceID(placeId) {
////                    place, error in
////                    completion(place, error)
////                }
////            } else {
////                completion(nil, error)
////            }
//        }
    }
    
    func getOpenStatus(of place: GMSPlace, completion: @escaping (( _ status: GMSPlacesOpenNowStatus?, _ error: Error?)->())) {
        GMSPlacesClient.shared().lookUpPlaceID(place.placeID) {
            place, error in
            completion(place?.openNowStatus, error)
        }
    }
    
    func getPhotos(GMSPlaceId: String, completion: @escaping ((_ images: [UIImage]?)->())) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: GMSPlaceId) {
            (photos, error) in
            if let photosMetadata = photos?.results {
                let photosToDownload = photosMetadata.count < 3 ? photosMetadata : Array(photosMetadata[0...2])
                var images = [UIImage]()
                var resultCounter = 0
                for photoToDownload in photosToDownload {
                    self.loadImageForMetadata(photoMetadata: photoToDownload) {
                        image in
                        resultCounter += 1
                        if let image = image {
                            images.append(image)
                        }
                        
                        if resultCounter == photosToDownload.count {
                            completion(images)
                        }
                    }
                }
                
            } else {
                completion(nil)
            }
        }
    }
    
    func getPhotos(from location: EALocation, completion: @escaping ((_ photos: [UIImage]?, _ error: Error?)->())) {
        let address = location.address
//        let coordinates = location.location ?? EALocation.defaultLocation
//        getPlaceInfo(query: address!, location: coordinates) {
//            place, error in
//            if let place = place {
//                self.getPhotos(GMSPlaceId: place.placeID) {
//                    images in
//                    completion(images, error)
//                }
//            } else {
//                completion(nil, error)
//            }
//        }
    }
    
    func getPlacemark(from location: CLLocation, completion: @escaping (( _ placemark: CLPlacemark?, _ error: Error?)->())) {
        
        CLGeocoder().reverseGeocodeLocation(location) {
            placemarks, error in
            let placemark = placemarks?.count ?? 0 > 0 ? placemarks![0] : nil
            
            completion(placemark, error)
        }
    }
    
    //MARK: Util
    fileprivate func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata, completion: @escaping ((_ image: UIImage?)->())) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            completion(photo)
        })
    }
}
