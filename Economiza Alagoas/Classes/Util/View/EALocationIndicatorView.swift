//
//  EALocationIndicatorView.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

class EALocationIndicatorView: UIView {
    @IBOutlet var contentView:UIView!
    @IBOutlet fileprivate var lineView: UIView!
    @IBOutlet fileprivate var activeLineView: UIView!
    @IBOutlet fileprivate var lineViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var activeLineViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var activeLineViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var activeLineLeadingConstraint: NSLayoutConstraint!
   
    fileprivate var lineViewDefaultLine: CGFloat!
    
    var numberOfSections: Int = 1 {
        didSet {
            activeLineViewWidthConstraint.constant = contentView.frame.width/CGFloat(numberOfSections)
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    private func initSubviews() {
        let nib = UINib(nibName: "EALocationIndicatorView", bundle: Bundle(for: EALocationIndicatorView.self))
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    //MARK: Public methods
    func setup(lineColor: UIColor, activeLineColor: UIColor, lineHeight: CGFloat, sections: Int) {
        numberOfSections = sections
        lineView.backgroundColor = lineColor
        activeLineView.backgroundColor = activeLineColor
        lineViewHeightConstraint.constant = lineHeight
        activeLineViewHeightConstraint.constant = lineHeight
        lineViewDefaultLine = lineHeight
    }
    
    /** 
     This will move the active line using the passed fraction. 
     For example, if there are 3 sections, a 1,2 fraction means its offset to the next anchor point will be 20%. If it is 2,4, it means its offset will be 40% to the third anchor point.
     - parameter fraction: The fraction from the current to the next anchor point.
     */
    func moveActiveLine(fraction: CGFloat) {
        activeLineLeadingConstraint.constant = contentView.frame.width*fraction/CGFloat(numberOfSections)
    }
    
    func hideTrailLine(_ hidden: Bool) {
        DispatchQueue.main.async {
            self.layoutIfNeeded()
            UIView.animate(withDuration: 0.3) {
                self.lineViewHeightConstraint.constant = hidden ? 1 : self.lineViewDefaultLine
                self.layoutIfNeeded()
            }
        }
    }
    
    func setLineAlpha(to alpha: CGFloat) {
        lineView.alpha = alpha
    }
}
