//
//  EAMapView.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit

protocol EAMapViewDelegate {
    func didUpdateLocation(to location: CLLocation)
    func getParentNavigationController() -> UINavigationController?
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
}

class EAMapView: UIView {
    @IBOutlet var contentView:UIView!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var collectionView: UICollectionView!

    let locationManager = CLLocationManager()
    var centerMapOnUserLocation:Bool = true
    let regionRadius: CLLocationDistance = 1000

    var locations:[EALocation]!
    var locationAnnotations:[EAMapAnnotation] = []
    
    fileprivate var selectedIndexPath: IndexPath!
    fileprivate var openLocationOnTap: Bool!
    var userLocation:CLLocation?
    var location = CLLocation(latitude: -9.652376, longitude: -35.701397)
    var selectedLocation: EALocation?
    var delegate: EAMapViewDelegate?
    
    fileprivate var pricesPerCNPJ: [String: Double]?
    fileprivate var isShowingGallery = true
    fileprivate var didLayoutSubviews = false
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    private func initSubviews() {
        let nib = UINib(nibName: "EAMapView", bundle: Bundle(for: EAMapView.self))
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
     
        if !didLayoutSubviews {
            didLayoutSubviews = true
        }
    }
    
    fileprivate func setupCollectionView() {
        collectionView.register(UINib(nibName: "EASearchResultCVC", bundle: Bundle(for: EASearchResultCVC.self)), forCellWithReuseIdentifier: "EASearchResultCVC")
        collectionView.dataSource = self
        collectionView.delegate = self
//        collectionView.isPagingEnabled = true
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = flowLayout
        collectionView.alwaysBounceHorizontal = true
        selectedIndexPath = IndexPath(row: 0, section: 0)
    }

    func setup(with locations: [EALocation], pricesPerCNPJ: [String: Double]?, delegate: EAMapViewDelegate, showGallery: Bool, openLocationOnTap: Bool) {
        setupCollectionView()
        self.locations = locations
        self.pricesPerCNPJ = pricesPerCNPJ
        self.openLocationOnTap = openLocationOnTap
        self.delegate = delegate
        self.isShowingGallery = showGallery
        if locations.count > 0 {
            self.selectedLocation = locations[0]
        }
        collectionView.reloadData()
        setupLocation()
        collectionView.isHidden = !showGallery
    }
    
    //MARK: Util
    private func setupLocation() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.showsBuildings = true
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        centerMapOnLocation(location: location)
        showVacancies()
    }
}

extension EAMapView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return locations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 100, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EASearchResultCVC.reuseId, for: indexPath) as! EASearchResultCVC
        let cnpj = locations[indexPath.row].cnpj
        let price = cnpj == nil ? nil : pricesPerCNPJ?[cnpj!]
        cell.setup(with: locations[indexPath.row], price: price)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 175, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        centerOn(locations[indexPath.row])
        scrollTo(index: indexPath.row)
        EANavigation.shared.showLocationScreen(locations[indexPath.row], inNavigationController: delegate?.getParentNavigationController())
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var row = Int(floor(collectionView.contentOffset.x/collectionView.frame.width))
        row = row < 0 ? 0 : row
        row = row > locations.count ? locations.count : row
        selectedIndexPath = IndexPath(row: row, section: 0) //TODO: ignore index 0 (empty space)
        centerOn(locations[row])
    }
}

extension EAMapView: CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations.last!
        EANavigation.shared.userLocation = userLocation
        location = CLLocation(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        delegate?.didUpdateLocation(to: location)
        if centerMapOnUserLocation {
            centerMapOnLocation(location: userLocation!)
            centerMapOnUserLocation = false
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return delegate?.mapView(mapView, viewFor: annotation)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let location = (view as? EAAnnotationView)?.location {
            selectedLocation = location
            centerOn(selectedLocation)
            if let index = locations.index(where: {$0.address == selectedLocation!.address}) {
                scrollTo(index: index)
            }
            
            if openLocationOnTap {
                EANavigation.shared.showLocationScreen(location)
            }
        }
    }
    
    //MARK: Util
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func centerOn(_ location: EALocation?) {
        guard location != nil else {
            return
        }
        if let latitude = location?.coordinates?.coordinate.latitude, let longitude = location?.coordinates?.coordinate.longitude {
            selectedLocation = location
            centerMapOnLocation(location: CLLocation(latitude: latitude, longitude: longitude))
        }
    }
    
    func scrollTo(index: Int) {
        selectedIndexPath = IndexPath(row: index, section: 0)
        guard index < collectionView.numberOfItems(inSection: 0) else {return}
        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: self.selectedIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    fileprivate func showVacancies() {
        mapView.removeAnnotations(locationAnnotations)
        locationAnnotations = self.locations.map ({
            (location: EALocation) -> EAMapAnnotation in
            return EAMapAnnotation(location: location, type: .regular)
        })
        mapView.addAnnotations(locationAnnotations)
    }
    
    
}
