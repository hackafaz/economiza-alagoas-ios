//
//  EAButton.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 24/04/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

@objc protocol EAButtonDelegate {
    func buttonStateChanged(_ button: EAButton, to isSelected: Bool)
    @objc optional func buttonWillExpand(_ button: EAButton)
    @objc optional func buttonDidCollapse(_ button: EAButton)
}

public enum EAIntensity {
    case high
    case medium
    case low
}

@IBDesignable
class EAButton: UIButton {
//    fileprivate var selectedImage: UIImage? = UIImage()
//    fileprivate var unselectedImage: UIImage? = UIImage()
    
    @IBInspectable var selectedImage: UIImage? {
        didSet {
            if isSelected, let image = selectedImage {
                setBackgroundImage(image, for: .normal)
            }
        }
    }
    
    @IBInspectable var unselectedImage: UIImage? {
        didSet {
            if !isSelected, let image = unselectedImage {
                setBackgroundImage(image, for: .normal)
            }
        }
    }
    
    @IBInspectable var disabledImage: UIImage? {
        didSet {
            if let disabledImage = disabledImage {
                setBackgroundImage(disabledImage, for: .disabled)
            }
        }
    }
    open var delegate: EAButtonDelegate?
//    var starType:Bool = false
    var previousSuperviewClipstoBounds: Bool? = false
    var animationIntensity: EAIntensity = .high
    var ignoreStateChanges: Bool = false
    var canTap = true
    
    override var isSelected: Bool {
        didSet {
            if let image = isSelected ? selectedImage : unselectedImage {
                setBackgroundImage(image, for: .normal)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    init(frame: CGRect, delegate: EAButtonDelegate, selectedImage: UIImage, unselectedImage: UIImage, disabledImage: UIImage? = nil) { // backgroundImage: UIImage, titleImage: UIImage) {
        super.init(frame: frame)
        initialize()
        self.delegate = delegate
        self.selectedImage = selectedImage
        self.unselectedImage = unselectedImage
        self.disabledImage = disabledImage
    }
    
    open func initialize() {
        initTouchEvents()
        adjustsImageWhenHighlighted = false
    }
    
    fileprivate func initTouchEvents() {
        addTarget(self, action: #selector(touchDown), for: .touchDown)
        addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        addTarget(self, action: #selector(touchDragExit), for: .touchDragExit)
    }
    
    //MARK: Touch events
    @objc fileprivate func touchDown() {
        canTap ? expand() : vibrate()
    }
    
    open func touchUpInside() {
        if canTap {
            setSelected(to: !isSelected)
            collapse()
        }
    }
    
    @objc fileprivate func touchDragExit() {
        if canTap {
            collapse()
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        if canTap {
            collapse()
        }
    }
    
    //MARK: Util
    func setImages(selected: UIImage?, unselected: UIImage?, disabled: UIImage?=nil) {
        selectedImage = selected
        unselectedImage = unselected
        disabledImage = disabled
        setEnabled(to: true)
    }
    
    func setEnabled(to enabled: Bool) {
        self.isEnabled = enabled
        titleLabel?.alpha = enabled ? 1 : 0
        if let image = enabled ? (isSelected ? selectedImage : unselectedImage) : disabledImage {
            setBackgroundImage(image, for: .normal)
        }
    }
    
    open func setSelected(to selected: Bool) {
        guard ignoreStateChanges == false else {
            delegate?.buttonStateChanged(self, to: isSelected)
            return
        }
        isSelected = selected
        titleLabel?.alpha = selected ? 1 : 0
        
        if let image = isSelected ? selectedImage : unselectedImage {
            setBackgroundImage(image, for: .normal)
        }
        delegate?.buttonStateChanged(self, to: isSelected)
    }
    
    //MARK: Animation
    func vibrate() {
        EAAnimation.vibrate(layer)
    }
    
    private func expand() {
        self.previousSuperviewClipstoBounds = self.superview?.clipsToBounds
        self.superview?.clipsToBounds = false
        self.delegate?.buttonWillExpand?(self)
        self.layer.removeAllAnimations()
        EAAnimation.expand(view: self, animationIntensity)
    }
    
//    private func expandFurther() {
//        layer.removeAllAnimations()
//        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: [], animations: {
//            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//        }) { _ in
//            self.collapse()
//        }
//    }
    
    private func collapse() {
        self.layer.removeAllAnimations()
        EAAnimation.animate(intensity: animationIntensity, delay: 0, {
            self.transform = CGAffineTransform.identity
        }) {
            self.delegate?.buttonDidCollapse?(self)
            self.superview?.clipsToBounds = self.previousSuperviewClipstoBounds ?? true
        }
    }
}
