//
//  EANavigation.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 17/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import BubbleTransition
import MapKit

class EANavigation: NSObject {
    static let shared = EANavigation()
    
    var bubbleTransition = BubbleTransition()
    /// The app's main navigationController
    var nav: UINavigationController!
    var main: EAMainVC!
    var userLocation: CLLocation?
    var bubbleCenter: CGPoint?
    var bubbleColor: UIColor?
    
    private override init() {
        bubbleTransition.duration = 0.3
    }
    
    func showSearch(completion: @escaping(()->())) {
        EANavigation.shared.nav.present(EASearchVC(), animated: true, completion: completion)
    }
    
    func showShareSheet(_ view: UIView, inViewController: UIViewController) {
        let sharingObjects: [AnyObject] = [UIImage(view: view), "Vê só o que encontrei no Appelou!" as AnyObject]
        let activityViewController = UIActivityViewController(activityItems: sharingObjects, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        inViewController.present(activityViewController, animated: true, completion: {})
    }
    
    func showLocationScreen(_ location: EALocation, inNavigationController: UINavigationController?=nil) {
        let locationVC: UIViewController = UINavigationController(rootViewController: EALocationVC(location))
        let nav = inNavigationController ?? EANavigation.shared.nav
        nav?.present(locationVC, animated: true, completion: nil)
    }
    
    func showProductScreen(_ product: EAProduct) {
        let productVC: UIViewController = UINavigationController(rootViewController: EAProductVC(product))
        EANavigation.shared.nav.present(productVC, animated: true, completion: nil)
    }
    
    func bubblePresent(_ viewController: UIViewController, center: CGPoint, color: UIColor) {
        viewController.modalPresentationStyle = .custom
        viewController.transitioningDelegate = self
        bubbleCenter = center
        bubbleColor = color
        EANavigation.shared.nav.present(viewController, animated: true, completion: nil)
    }
    
    func openInMaps(name: String?, coordinates: CLLocationCoordinate2D) {
        let regionDistance:CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
}

extension EANavigation: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        bubbleTransition.transitionMode = .present
        bubbleTransition.startingPoint = bubbleCenter ?? CGPoint.zero
        bubbleTransition.bubbleColor = bubbleColor ?? .white
        return bubbleTransition
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        bubbleTransition.transitionMode = .dismiss
        bubbleTransition.startingPoint = bubbleCenter ?? CGPoint.zero
        bubbleTransition.bubbleColor = bubbleColor ?? .white
        return bubbleTransition
    }
}
