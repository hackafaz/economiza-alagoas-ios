//
//  EAAnimation.swift
//  Economiza Alagoas
//
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

public class EAAnimation {
    private init() {}
    
    //MARK: Specific Animations available
    /**
     Performs a nudge-like animation indication the operation cannot be performed.
     */
    static func vibrate(_ layer: CALayer) {
        layer.removeAllAnimations()
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.3
        animation.values = [-20.0, 20.0, -5.0, 5.0, -3.0, 3.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    static func expand(view: UIView, _ intensity: EAIntensity) { //, completion: (()->()))) {
        var scaleFactors: (CGFloat, CGFloat) = (1, 1)
        
        switch intensity {
        case .high:
            scaleFactors = (1.3, 1.3)
        case .medium:
            scaleFactors = (1.22, 1.22)
        case .low:
            scaleFactors = (1.15, 1.15)
        }
        
        EAAnimation.animate(intensity: .high, delay: 0, {
            view.transform = CGAffineTransform(scaleX: scaleFactors.0, y: scaleFactors.1)
        })
    }
    
    //MARK: General
    static func animate(intensity: EAIntensity, delay: CGFloat, _ animationClosure: @escaping (()->()), _ completion:(()->())?=nil) {
        let params = getSpringAnimationParams(intensity, withDelay: delay)
        UIView.animate(withDuration: TimeInterval(params[0]), delay: TimeInterval(params[1]), usingSpringWithDamping: params[2], initialSpringVelocity: params[3], options: [], animations: {
            animationClosure()
        }) { _ in
            completion?()
        }
    }
    
    static func animate(intensity: EAIntensity, duration: CGFloat, delay: CGFloat, _ animationClosure: @escaping (()->()), _ completion:(()->())?=nil) {
        let params = getSpringAnimationParams(intensity, withDelay: delay)
        UIView.animate(withDuration: TimeInterval(duration), delay: TimeInterval(params[1]), usingSpringWithDamping: params[2], initialSpringVelocity: params[3], options: [], animations: {
            animationClosure()
        }) { _ in
            completion?()
        }
    }
    
    //MARK: Util
    static func getSpringAnimationParams(_ forIntensity: EAIntensity, withDelay: CGFloat) -> [CGFloat] {
        var params = [CGFloat]()
        switch forIntensity {
        case .high:
            params = [0.4, withDelay, 0.3, 11]
        case .medium:
            params = [0.4, withDelay, 0.3, 7]
        case .low:
            params = [0.6, withDelay, 0.8, 3]
        }
        return params
    }
}
