//
//  BooleanExtension.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 20/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation


extension Bool {
    static func random() -> Bool {
        return arc4random_uniform(2) == 0
    }
}
