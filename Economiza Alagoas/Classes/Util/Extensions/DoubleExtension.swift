//
//  DoubleExtension.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation

extension Double {
    var price: String {
        return "R$\(self.rounded(2) ?? 0)"
    }
    
    /// Rounds the double to decimal places value
    func rounded(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
