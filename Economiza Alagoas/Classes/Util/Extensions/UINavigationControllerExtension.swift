//
//  UINavigationControllerExtensino.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 13/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    func EAAppearance() {
        self.navigationBar.isTranslucent = false
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = UIColor(rgba: "#CC7E85")
        self.navigationBar.barTintColor = UIColor(rgba: "#FEFCFB")
    }
}
