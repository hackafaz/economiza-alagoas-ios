//
//  UIViewExtension.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func roundCorners(_ radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func addShadow(intensity: EAIntensity) {
        layer.shadowColor = UIColor.black.cgColor
        if intensity == .low {
            layer.shadowOffset = CGSize(width: 0, height: 1);
            layer.shadowOpacity = 0.1
            layer.shadowRadius = 1
        } else if intensity == .medium {
            layer.shadowOffset = CGSize(width: 0, height: 1);
            layer.shadowOpacity = 0.15
            layer.shadowRadius = 1
        } else {
            layer.shadowOffset = CGSize(width: 0, height: 2);
            layer.shadowOpacity = 0.15
            layer.shadowRadius = 2
        }
    }

}
