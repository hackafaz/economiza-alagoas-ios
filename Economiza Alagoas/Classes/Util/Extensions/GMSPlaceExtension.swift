//
//  GMSPlaceExtension.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import GooglePlaces
import GooglePlacePicker


extension GMSPlace {
    func getOpenNowFormatted() -> (open: Bool?, status: String) {

        var open: Bool?
        var status: String!
        
        let openFake = Bool.random()
        if openFake {
            status = "Aberto"
            open = true
        } else {
            status = "Fechado"
            open = false
        }
        return (open, status)
        
    }
//        var open: Bool?
//        var status: String!
//        switch self.openNowStatus {
//        case .no:
//            status = "Fechado"
//            open = false
//        case .yes:
//            status = "Aberto"
//            open = true
//        default:
//            status = "..."
//        }
//        return (open, status)
//    }
}
