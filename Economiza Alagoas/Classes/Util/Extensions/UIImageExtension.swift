//
//  UIImageExtension.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 19/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
    
    convenience init(scrollView: UIScrollView) {
        let scale: CGFloat = 4
        UIGraphicsBeginImageContextWithOptions(scrollView.bounds.size, false, scale)
        let offset =  scrollView.contentOffset
        UIGraphicsGetCurrentContext()!.translateBy(x: -offset.x, y: -offset.y)
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}
