//
//  EAImageUtil.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import CollieGallery

class EAImageUtil {
    
    static func getDefaultCollieGalleryOptions() -> CollieGalleryOptions {
        let options = CollieGalleryOptions()
        CollieGalleryOptions.sharedOptions.gapBetweenPages = 0
        options.gapBetweenPages = CollieGalleryOptions.sharedOptions.gapBetweenPages
        options.preLoadedImages = CollieGalleryOptions.sharedOptions.preLoadedImages
        options.showProgress = CollieGalleryOptions.sharedOptions.showProgress
        options.showCloseButton = false
        options.enableSave = true
        options.enableInteractiveDismiss = true
        return options
    }
}
