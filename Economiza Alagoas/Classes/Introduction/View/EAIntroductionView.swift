

import UIKit
import ISScrollViewPageSwift
import IlhasoftCore

class EAIntroductionView: UIView {

    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!

    //viewOne subviews
    @IBOutlet weak var view1back: UIView!
    @IBOutlet weak var view1Star: UIImageView!
    @IBOutlet weak var view1Icon: UIImageView!
    
    @IBOutlet weak var view2Real: UIView!
    
    //viewTwo subviews
    @IBOutlet weak var view2back: UIView!
    @IBOutlet weak var view2star: UIImageView!
    @IBOutlet weak var view2play: UIImageView!
    
    //viewThree subviews
    @IBOutlet weak var view3back: UIView!
    @IBOutlet weak var view3benta: UIImageView!
    @IBOutlet weak var view3star: UIImageView!
    @IBOutlet weak var btLetsGo: EAButton!
    
    var didLayoutSubview = false
    var currentIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btLetsGo.animationIntensity = .high
        btLetsGo.ignoreStateChanges = true 
        //teste.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !didLayoutSubview {
//            scrollView.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
//            setupScrollView()
            didLayoutSubview = true
            btLetsGo.roundCorners(10)
        }
    }
    
    class func createInstance(frame:CGRect, screen: Int) -> EAIntroductionView {
        let view = Bundle.main.loadNibNamed("EAIntroductionView", owner: self, options: nil)?[0] as! EAIntroductionView
        view.frame = frame
        view.setupViews(screen: screen)
        return view
    }

    func setupViews(screen: Int) {
        viewOne.isHidden = screen != 1
        viewTwo.isHidden = screen != 2
        view2Real.isHidden = screen != 4
        viewThree.isHidden = screen != 3
    }

    @IBAction func btLetsGoDidTap(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "EADidShowTutorial")
        EANavigation.shared.main = EAMainVC()
        EANavigation.shared.nav = UINavigationController(rootViewController: EANavigation.shared.main)
        
        switchRootViewController(EANavigation.shared.nav, animated: true, completion: nil)
    }
}

