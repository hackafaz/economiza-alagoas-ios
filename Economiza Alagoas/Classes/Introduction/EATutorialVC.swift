

import UIKit
import IlhasoftCore
import ISScrollViewPageSwift

class EATutorialVC: UIViewController,ISScrollViewPageDelegate {
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var tutorialScrollView: ISScrollViewPage!
    
    var didLayoutSubview = false
    var currentIndex = 0
    let scrollViewPagingFactor:CGFloat = 3
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setupLayout() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isHidden = true
        self.pgControl.numberOfPages = 4
        self.pgControl.currentPage = 0
    }
    
    func setupScrollViewPage() {
        tutorialScrollView.setPaging(true)
        tutorialScrollView.scrollViewPageType = ISScrollViewPageType.horizontally
        self.tutorialScrollView.scrollViewPageDelegate = self
        let height = self.view.bounds.size.height - 60
        for i in 0...3 {
            
            switch i {
            case 0:
                let view1 = EAIntroductionView.createInstance(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: height), screen: 1)
                tutorialScrollView.addCustomView(view1)
                break
            case 1:
                let view4 = EAIntroductionView.createInstance(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: height), screen: 4)
                tutorialScrollView.addCustomView(view4)
            case 2:
                let view2 = EAIntroductionView.createInstance(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: height), screen: 2)
                tutorialScrollView.addCustomView(view2)
                break
            case 3:
                let view3 = EAIntroductionView.createInstance(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: height), screen: 3)
                tutorialScrollView.addCustomView(view3)
                break
            default:
                break
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !didLayoutSubview {
            setupScrollViewPage()
            didLayoutSubview = true
        }
    }
    
    func animateButton(goDown: Bool) {
        self.view.layoutIfNeeded()
        //self.constraintHeightViewLogin.constant = goDown == true ? 120 : 19
        
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
        }
    }    
    
    func setupTutorialStepByIndex(currentIndex:Int) {
        self.pgControl.currentPage = currentIndex
    }
    
    // MARK: Button Actions
    
    //MARK: ISScrollViewPageDelegate
    func scrollViewPageDidChanged(_ scrollViewPage: ISScrollViewPage, index: Int) {
        currentIndex = index
        setupTutorialStepByIndex(currentIndex: currentIndex)
//        if currentIndex == 0 {
//            let View2 = self.tutorialScrollView.views[1] as! EAIntroductionView
//            View2.view2back.frame = CGRect(x: 0, y: View2.view2back.frame.minY , width: View2.view2back.bounds.width, height: View2.view2back.bounds.width)
//            View2.view2play.frame = CGRect(x: 0, y: View2.view2play.frame.minY , width: View2.view2play.bounds.width , height: View2.view2play.bounds.height)
//        }else if currentIndex == 1 {
//            let view = self.tutorialScrollView.views[2] as! EAIntroductionView
//            view.view3back.frame = CGRect(x: 0, y: view.view3back.frame.minY, width: view.view3back.bounds.width, height: view.view3back.bounds.height)
//            view.view3star.frame = CGRect(x: 0, y: view.view3star.frame.minY, width: view.view3star.bounds.width, height: view.view3star.bounds.height)
//            view.view3back.frame = CGRect(x: 0, y: view.view3back.frame.minY , width: view.view3back.bounds.width , height: view.view3back.bounds.height)
//        }else {
//
//        }
    }
    
    
}
