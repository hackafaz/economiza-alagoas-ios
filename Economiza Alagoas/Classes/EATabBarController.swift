//
//  EATabBarController.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

class EATabBarController: UITabBarController {
    static var shared = EATabBarController()
    fileprivate(set) public var mainVC = EAMainVC()
    fileprivate(set) public var searchVC = UIViewController()
    fileprivate(set) public var settingsVC = UIViewController()
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
    }
    
    fileprivate func setupTabBar() {
        mainVC.title = "Você"
//        mainVC.tabBarItem.image = UIImage()
        searchVC.title = "Busca"
//        searchVC.tabBarItem.image = UIImage()
        settingsVC.title = "Ajustes"
//        settingsVC.tabBarItem.image = UIImage()
        viewControllers = [mainVC, searchVC, settingsVC]
        tabBar.isTranslucent = false
        tabBar.barTintColor = UIColor(rgba: "#FEFCFB")
        tabBar.tintColor = UIColor(rgba: "#8e6d7e")
    }
}
