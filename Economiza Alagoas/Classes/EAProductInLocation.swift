//
//  EAProductInLocation.swift
//  Economiza Alagoas
//
//  Created by Rubens Pessoa on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import RealmSwift

class EAProductInLocation: Object {
    dynamic var product: EAProduct!
    dynamic var location: EALocation!
    let price = RealmOptional<Double>()
    dynamic var lastSaleDate: Date!
}
