//
//  EAProduct.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class EAProduct: Object {
    dynamic var name: String? = UUID().uuidString
    dynamic var barCode: String?
    dynamic var lastSaleDate: Date?
    let averagePrice = RealmOptional<Double>()
    let bestPrice = RealmOptional<Double>()
    let worstPrice = RealmOptional<Double>()
    
    override static func primaryKey() -> String? {
        return "barCode"
    }
}
