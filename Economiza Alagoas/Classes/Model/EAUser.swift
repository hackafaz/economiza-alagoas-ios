//
//  EAUser.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import RealmSwift

class EAUser {
    static let shared = EAUser()
    static let realm = try! Realm()
    
    static func current() -> EAUser {
        return EAUser.shared
    }
    
    func getProducts() -> [EAProduct] {
        return Array(EAUser.realm.objects(EAProduct.self))
    }
    
    func getLocations() -> [EALocation] {
        return Array(EAUser.realm.objects(EALocation.self))
    }
    
    func saveFavorite(product: EAProduct, completion: (Bool) -> ()) {
        do {
            try EAUser.realm.write {
                if let response = EAUser.realm.object(ofType: EAProduct.self, forPrimaryKey: product.barCode) as? EAProduct {
                    completion(false)
                } else {
                    EAUser.realm.add(product)
                    completion(true)
                }
            }
        } catch {
            completion(false)
        }
    }
    
    func saveFavorite(location: EALocation, completion: (Bool) -> ()) {
        do {
            try EAUser.realm.write {
                if let response = EAUser.realm.object(ofType: EALocation.self, forPrimaryKey: location.cnpj) as? EALocation {
                    completion(false)
                } else {
                    EAUser.realm.add(location)
                    completion(true)
                }
            }
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    func removeFavorite(product: EAProduct, completion: (Bool) -> ()) {
        do {
            try EAUser.realm.write {
                EAUser.realm.delete(EAUser.realm.object(ofType: EAProduct.self, forPrimaryKey: product.barCode)!)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
    
    func removeFavorite(location: EALocation, completion: (Bool) -> ()) {
        do {
            try EAUser.realm.write {
                EAUser.realm.delete(EAUser.realm.object(ofType: EALocation.self, forPrimaryKey: location.cnpj)!)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
}
