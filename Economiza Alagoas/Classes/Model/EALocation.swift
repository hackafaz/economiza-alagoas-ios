//
//  EALocation.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import MapKit
import RealmSwift

class EALocation: Object {
    static var defaultLocation = CLLocation(latitude: -9.652376, longitude: -35.701397)
    dynamic var name: String?
    dynamic var phone: String?
    dynamic var latitude: String?
    dynamic var longitude: String?
    dynamic var cnpj: String?
    dynamic var address: String?
    
    var coordinates: CLLocation? {
        get {
            var location: CLLocation?
            if let lat = latitude, let latDg = CLLocationDegrees(lat), let long = longitude, let longDg = CLLocationDegrees(long) {
                location = CLLocation(latitude: latDg, longitude: longDg)
            }
            return location
        }
    }
    
    func getImages(completion: ((_ images: [UIImage]?)->())) {
        completion(nil)
    }
    
    static var available: CLLocation {
        get {
            return EANavigation.shared.main.mapView.userLocation ?? EALocation.defaultLocation
        }
    }
    
//    static let fakes = [
//        EALocation(name: "Palato", location: CLLocationCoordinate2D(latitude: -9.650692, longitude: -35.709658), address: "R Dep Jose Lages 700 Maceio AL", phone: "2123-8888", email: nil),
//        EALocation(name: "Palato", location: CLLocationCoordinate2D(latitude: -9.653305, longitude: -35.712694), address: "Av. Fernandes Lima, 5482 Maceio AL", phone: "2123-8888", email: nil),
//        EALocation(name: "Palato", location: CLLocationCoordinate2D(latitude: -9.651232, longitude: -35.717179), address: "R. Comendador Palmeira, 2186 Maceio AL", phone: "2123-8888", email: nil),
//        EALocation(name: "Palato", location: CLLocationCoordinate2D(latitude: -9.657155, longitude: -35.722554), address: "R. Comendador Palmeira, 2856", phone: "2123-8888", email: nil),
//        EALocation(name: "Palato", location: CLLocationCoordinate2D(latitude: -9.660661, longitude: -35.717595), address: "R. Comendador Palmeira, 2826", phone: "2123-8888", email: nil),
//        EALocation(name: "Palato", location: CLLocationCoordinate2D(latitude: -9.660227, longitude: -35.721962), address: "R. Comendador Palmeira, 1286", phone: "2123-8888", email: nil),
//        ]
    override static func primaryKey() -> String? {
        return "cnpj"
    }
}
