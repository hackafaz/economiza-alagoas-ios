//
//  EASefazNFCeAPI.swift
//  Economiza Alagoas
//
//  Created by Rubens Pessoa on 18/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import MapKit
import Alamofire
import ObjectMapper
import RealmSwift

class EASefazAPI {
    
    static let shared = EASefazAPI()
    
    let headers = ["Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyNDA1MjQ3MSIsImF1dGgiOiJST0xFX0VNSV9FWFRSQVRPLFJPTEVfUkVMX0RFTlVOX0VTUE9OVEFORUEsUk9MRV9FTUlfQ1AsUk9MRV9SRUxfT01JU1NBTyIsImlkQ29uZXhhbyI6IjNFNDJCMjUyRDc0QjQyMjMxOUMzMkY0N0U4MkJFQjREIiwibnVtUGVzc29hIjozNjQ3LCJpbmRTdGF0dXMiOiJBIiwiaWRBcGxpY2F0aXZvIjoyNywiaWRBdXRvcml6YWNhbyI6NzUsImV4cCI6MTUxODkxMjAwMH0.OJq_KD73fWOqkc5apUHREoGJ8MKlljkpKu70oUxYDSwdFXUW-synxJvel5VKIdre8cBQLw-QDbEo8vLasrfzZg"]
    
    /**
     Get products from the SEFAZ API.
     
     - parameters:
        - byDescription: The text which the user wants to search. Cannot be empty.
        - days: The quantity of days the user wants to search for. Must be 0 or less than 4, default value is 3. Can be empty.
        - coordinates: The location in which the user wants to perfom a geolocation search. Cannot be empty.
        - radius: The distance radius from the user location which the user wants to look for products. Can be empty
        - completion: A callback method in which the user retrieves an Array of EAProduct objects as a response for the call. Cannot be empty.
     */
    func getProducts(byDescription: String!, days: Int? = 3, coordinates: CLLocation!, radius: Int? = 10, completion: @escaping ([EAProduct]?) -> ()) {
        let coordinates = CLLocationCoordinate2D(latitude: coordinates.coordinate.latitude, longitude: coordinates.coordinate.longitude)
        var parameters = self.createParameters(days: days, coordinates: coordinates, radius: radius)
        parameters?["descricao"] = byDescription
        Alamofire.request("http://hackathonapi.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosPorDescricao", method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            case .success(let objects):
                let mappedObjects: [EAProduct]? = self.filterProducts(response: objects as? [[String : Any]])
                completion(mappedObjects)
            case .failure(let error):
                print("ALERT DEBUG:  \(error.localizedDescription)")
                completion(nil)
            }
        }
    }
    
    /**
     Get locations selling a product.
     
     - parameters:
        - product: The product the user wants to look for. Cannot be empty.
        - completion: A callback method in which the user retrieves an Array of EALocation and a Dictionary with CNPJ as keys and Prices as value as a response for the call. Cannot be empty.
     */
    func getLocationsSelling(_ product: EAProduct, completion: @escaping (_ locations: [EALocation]?, _ pricesPerCNPJ: [String: Double]?)->()) {
        let coordinates = EALocation.available
        getProducts(byBarCode: product.barCode!, coordinates: coordinates, radius: 15) {
            result in
            if let locations = result?["locations"] as? [EALocation], let prices = result?["pricePerCNPJ"] as? [String: Double] {
                completion(locations, prices)
            } else {
                completion(nil, nil)
            }

        }
    }
    
    /**
     Get products from the SEFAZ API.
     
     - parameters:
        - byBarCode: The bar code which the user wants to search. Cannot be empty.
        - days: The quantity of days the user wants to search for. Must be 0 or less than 4, default value is 3. Can be empty.
        - coordinates: The location in which the user wants to perfom a geolocation search. Cannot be empty.
        - radius: The distance radius from the user location which the user wants to look for products. Can be empty
        - completion: A callback method in which the user retrieves a Dictionary as a response for the call. Cannot be empty.
     */
    func getProducts(byBarCode: String!, days: Int? = 3, coordinates: CLLocation!, radius: Int? = 10, completion: @escaping ([String: Any]?) -> ()) {
        let coordinates = CLLocationCoordinate2D(latitude: coordinates.coordinate.latitude, longitude: coordinates.coordinate.longitude)
        var parameters = self.createParameters(days: days, coordinates: coordinates, radius: radius)
        parameters?["codigoDeBarras"] = byBarCode
        
        Alamofire.request("http://hackathonapi.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosPorCodigoDeBarras", method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            case .success(let objects):
                var filteredObjs = self.filter(objects: objects as? [[String : Any]])
                
                var finalResponse: [String: Any] = [:]
                
                finalResponse["product"] = filteredObjs["product"] 
                finalResponse["locations"] = self.mapObjects(listResponse: filteredObjs["filtered"] as? [[String : Any]])
                finalResponse["pricePerCNPJ"] = filteredObjs["pricePerCNPJ"]
                
                completion(finalResponse)
            case .failure(let error):
                print("ALERT DEBUG:  \(error.localizedDescription)")
                completion(nil)
            }
        }
    }
    
    /**
     Get the price for user favorite products in a given location.
     
     - parameters:
        - cnpj: The number that represents locations solely. Cannot be empty.
        - location: The location in which the user wants to perfom a geolocation search. Cannot be empty.
        - completion: A callback method in which the user retrieves a Dictionary as a response for the call. Cannot be empty.
     */
    func getFavoriteProductsInLocation(cnpj: String, location: CLLocation? = EALocation.available, completion: @escaping ((_ products: [EAProduct: Double])->())) {
        let realm = try! Realm()
        let favorites = Array(realm.objects(EAProduct.self))
        
        var products: [EAProduct: Double] = [:]
        var counter = 0
        for favorite in favorites {
            counter += 1
            self.getProducts(byBarCode: favorite.barCode!, coordinates: location, completion: {
                response in
                if let prod = response!["product"] as? EAProduct, let price = (response!["pricePerCNPJ"] as? [String: Double])?[cnpj] {
                    products[prod] = price
                }
                
                if counter >= favorites.count {
                    completion(products)
                }
            })
        }
    }
    
    private func createParameters(days: Int?, coordinates: CLLocationCoordinate2D?, radius: Int?) -> [String: Any]! {
        
        var parameters: [String: Any] = [:]
    
        if days != nil {
            parameters["dias"] = "\(String(describing: days!))"
        }
        
        if coordinates != nil {
            parameters["latitude"] = "\(String(describing: coordinates!.latitude))"
            parameters["longitude"] = "\(String(describing: coordinates!.longitude))"
        }
        
        if radius != nil {
            parameters["raio"] = "\(String(describing: radius!))"
        }
        
        return parameters
    }
    
    private func filterProducts(response: [[String: Any]]?) -> [EAProduct] {
        guard let response = response else {return []}
        
        var productList: [EAProduct] = []
        for json in response {
            let product = EAProduct()
            
            if let barCode = json["codGetin"] as? String {
                product.barCode = barCode
                
                if let name = json["dscProduto"] as? String {
                    product.name = name
                }
                
                productList.append(product)
            }
        }
        
        return productList
    }
    
    private func mapObjects(listResponse: [[String: Any]]?) -> [EALocation]? {
        guard let listResponse = listResponse else {return []}
        var locationList: [EALocation] = []
        
        for response in listResponse {
            let location = EALocation()
            
            if let name = response["nomFantasia"] as? String {
                location.name = name
            } else if let name = response["nomRazaoSocial"] as? String {
                location.name = name
            }
            
            if let phone = response["numTelefone"] as? String {
                location.phone = phone
            }
            
            if let latitude = response["numLatitude"] as? Double {
                location.latitude = "\(latitude)"
            }
            
            if let longitude = response["numLongitude"] as? Double {
                location.longitude = "\(longitude)"
            }
            
            if let cnpj = response["numCNPJ"] as? String {
                location.cnpj = cnpj
            }
            
            if let street = response["nomLogradouro"] as? String, let number = response["numImovel"] as? String, let neighborhood = response["nomBairro"] as? String {
                
                location.address = "\(street.condensedWhitespace.capitalized), \(String(describing: number)) - \(neighborhood.condensedWhitespace.capitalized)"
            }
            
            locationList.append(location)
        }
        
        return locationList
    }
    
    private func filter(objects: [[String: Any]]?) -> [String: Any] {
        guard let objects = objects else {return [:]}
        var response: [String: Any] = [:]
        var prices = [Double]()
        var sumPrices: Double = 0.0
        var product = EAProduct()
        var firstObj = objects.count > 0 ? objects[0] as? [String: Any] : [:]
        var pricePerCNPJ: [String: Double] = [:]
        
        if let name = firstObj?["dscProduto"] as? String {
            product.name = name
        }
        
        if let barCode = firstObj?["codGetin"] as? String {
            product.barCode = barCode
        }
        
        for object in objects {
            let obj = object as? [String: Any]
            if let price = obj?["valUnitarioUltimaVenda"] as? Double {
                prices.append(price)
                sumPrices += price
            }
        }
        
        let standardDeviation = self.standardDeviation(arr: prices)
        let averagePrice = sumPrices/Double(prices.count)
        var filtered: [[String: Any]] = [[:]]
        
        for object in objects {
            let obj = object as? [String: Any]
            if let price = obj?["valUnitarioUltimaVenda"] as? Double {
                if abs(price - averagePrice) < (1.5 * standardDeviation) {
                    filtered.append(obj!)
                }
            }
        }
        
        var filteredPrices = [Double]()
        
        for object in objects {
            let obj = object as? [String: Any]
            if let cnpj = obj?["numCNPJ"] as? String {
                if let price = obj?["valUnitarioUltimaVenda"] as? Double {
                    filteredPrices.append(price)
                    pricePerCNPJ[cnpj] = price
                }
            }
        }

        product.averagePrice.value = averagePrice
        product.bestPrice.value = filteredPrices.min()
        product.worstPrice.value = filteredPrices.max()
        
        response["filtered"] = filtered
        response["product"] = product
        response["pricePerCNPJ"] = pricePerCNPJ

        return response
    }
    
    private func standardDeviation(arr : [Double]) -> Double {
        let length = Double(arr.count)
        let avg = arr.reduce(0, {$0 + $1}) / length
        let sumOfSquaredAvgDiff = arr.map { pow($0 - avg, 2.0)}.reduce(0, {$0 + $1})
        return sqrt(sumOfSquaredAvgDiff / length)
    }

}
