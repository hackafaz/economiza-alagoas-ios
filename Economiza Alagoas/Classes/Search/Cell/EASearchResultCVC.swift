//
//  EASearchResultCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import IlhasoftCore

class EASearchResultCVC: UICollectionViewCell {
    static let reuseId: String = "EASearchResultCVC"
   
    @IBOutlet var gradientView: GradientView!
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbAddress: UILabel!
    
    @IBOutlet var btCar: UIButton!
    @IBOutlet var btOpen: UIButton!
    @IBOutlet var lbBestPrice: UILabel!
    @IBOutlet var lbNumber: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var lbDistance: UILabel!
    
    var location: EALocation!
    
    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        gradientView.roundCorners(10)
        btCar.roundCorners(btCar.frame.width/2)
        btOpen.roundCorners(7)
    }
    
    func setup(with location: EALocation, price: Double?) {
        self.location = location

        if let loc = self.location.coordinates {
            if let userLoc = EANavigation.shared.userLocation {
                let dist = String(format: "%.2f", (userLoc.distance(from: loc))/1000)
                lbDistance.text = "\(dist)km distante"
            }
        } else {
            lbDistance.text = ""
        }
        
        lbName.text = location.name?.localizedUppercase
        lbBestPrice.isHidden = price == nil
        lbBestPrice.text = price?.price
        lbAddress.text = location.address
        lbNumber.text = location.phone
        
        btOpen.isHidden = true
//        let isOpen = Bool.random()
//        setBtOpen(to: isOpen)
    }
    
    fileprivate func setBtOpen(to isOpen: Bool) {
        let hexa = isOpen ? "#EAFAFF" : "#FFEAEA"
        btOpen.backgroundColor = UIColor(rgba: hexa)
        btOpen.setTitle(isOpen ? "Aberto" : "Fechado", for: .normal)
    }
    
    //MARK: Touch
    @IBAction func btCarDidTouch(_ sender: UIButton) {
        if let location = location, let coordinates = location.coordinates?.coordinate {
            EANavigation.shared.openInMaps(name: location.name, coordinates: coordinates)
        } else {
            let topVC = EANavigation.shared.nav.topViewController!
            ISAlertMessages.displayMessage("Não encontramos a localização... pode ir direto no Google Maps?", withTitle: "Cadê as coordenadas?", fromController: topVC)
        }
        //TODO: car route
    }
}
