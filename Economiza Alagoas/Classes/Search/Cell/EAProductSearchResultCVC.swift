//
//  EAProductSearchResultCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit


class EAProductSearchResultCVC: UICollectionViewCell {
    static let reuseId: String = "EAProductSearchResultCVC"
    
    @IBOutlet var lbName: UILabel!
    @IBOutlet var btHeart: EAButton!
    fileprivate var product: EAProduct!
    var items = [Any]()
    
    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        btHeart.animationIntensity = .high
    }
    
    func setup(with product: EAProduct) {
        self.product = product
        lbName.text = product.name
        if let prod = EAUser.realm.object(ofType: EAProduct.self, forPrimaryKey: product.barCode) as? EAProduct {
            btHeart.setSelected(to: true)
        } else {
            btHeart.setSelected(to: false)
        }
    }
    @IBAction func btHeartDidTap(_ sender: Any) {
        
        if btHeart.isSelected {
            EAUser.current().saveFavorite(product: product) {
                success in
                EANotification.newFavoriteProduct()
            }
        } else {
            EAUser.current().removeFavorite(product: product) {
                success in
                EANotification.newFavoriteProduct()
            }
        }
        
    }
}
