//
//  EASearchVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit
import IlhasoftCore
import MBProgressHUD

class EASearchVC: GAITrackedViewController, UICollectionViewDelegate {
    @IBOutlet var radiusSlider: UISlider!
    @IBOutlet var lbRadius: UILabel!
    @IBOutlet var btBarcode: EAButton!
    fileprivate var btSearch: UIBarButtonItem!
    fileprivate var btExit: UIBarButtonItem!
    
    @IBOutlet var tfSearch: UITextField!
    fileprivate var radius: Float!
    fileprivate var barCode: String?
    fileprivate var goToCamera = false
    fileprivate var isSearching = false
    //MARK: Init
    init(goToCamera: Bool = false) {
        super.init(nibName: "EASearchVC", bundle: nil)
        self.goToCamera = goToCamera
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = EAScreens.search.rawValue
        setupLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
        tfSearch.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        EAAnalytics.shared.track(.search)
        
        if goToCamera {
            btScannerDidTap(UIButton())
            goToCamera = false
        }
    }
    
    fileprivate func setupLayout() {
        radius = 6
        radiusSlider.setValue(6, animated: true)
        btBarcode.animationIntensity = .high
        btBarcode.roundCorners(btBarcode.frame.width/2)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        tfSearch.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        tfSearch.delegate = self
    }
    
    fileprivate func startSearch() {
        guard let searchWord = tfSearch.text, !isSearching else {
            return
        }
        isSearching = true
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if !searchWord.isNumeric {
            EASefazAPI.shared.getProducts(byDescription: tfSearch.text!, coordinates: EALocation.available, radius: Int(self.radius)) {
                result in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.isSearching = false
                if let products = result {
                    let productListVC = EASearchProductsListVC(products)
                    self.navigationController?.pushViewController(productListVC, animated: true)
                } else {
                    ISAlertMessages.displayMessage("Pode tentar novamente jajá?", withTitle: "Erro no servidor", fromController: self)
                }
            }
        } else {
            EASefazAPI.shared.getProducts(byBarCode: tfSearch.text!, coordinates: EALocation.available, radius: Int(radius)) {
                result in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.isSearching = false
                if let product = result?["product"] as? EAProduct, let _ = result?["locations"] as? [EALocation] {
                    let productListVC = EASearchProductsListVC([product])
                    self.navigationController?.pushViewController(productListVC, animated: true)
                } else {
                    ISAlertMessages.displayMessage("Parece que ocorreu algo de errado. Vamos tentar novamente?", withTitle: "Hum....", fromController: self)
                }
            }
        }
    }
    
    //MARK: Touch
    @objc fileprivate func hideKeyboard() {
        tfSearch.resignFirstResponder()
    }
    
    @IBAction func sliderDidChangeValue(_ sender: UISlider) {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        let radiusStr = formatter.string(from: NSNumber(value: sender.value))!
        radius = (sender.value)
        lbRadius.text = "\(radiusStr)km"
    }
    
    @IBAction func btScannerDidTap(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.tfSearch.resignFirstResponder()
            let codeScannerVC = EABarCodeScannerVC(self)
            self.navigationController?.pushViewController(codeScannerVC, animated: true)
        }
    }
    
    @IBAction func btExitDidTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension EASearchVC: EABarCodeScannerDelegate {
    func didRead(barCode: String) {
        tfSearch.text = (barCode)
        startSearch()
    }
}

extension EASearchVC: UITextFieldDelegate {
    @objc fileprivate func textFieldDidChange(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cleanString = textField.text?.clean
        
        var should = true
        if string == "\n" {
            if cleanString?.characters.count ?? 0 >= 4 {
                startSearch()
                textField.resignFirstResponder()
            }
            should = false
        }
        return should
    }
}
