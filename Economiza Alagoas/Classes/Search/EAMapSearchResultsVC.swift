//
//  EAMapSearchResultsVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 19/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit

class EAMapSearchResultsVC: UIViewController {
    @IBOutlet var mapView: EAMapView!
    @IBOutlet var btExit: UIButton!
    
    var locations: [EALocation]!
    var pricesPerCNPJ: [String: Double]?
    
    init(_ locations: [EALocation], pricesPerCNPJ: [String: Double]?) {
        super.init(nibName: "EAMapSearchResultsVC", bundle: nil)
        self.locations = locations
        self.pricesPerCNPJ = pricesPerCNPJ
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.setup(with: locations, pricesPerCNPJ: pricesPerCNPJ, delegate: self, showGallery: true, openLocationOnTap: false)
        btExit.roundCorners(btExit.frame.width/2)
    }
    
    @IBAction func btExitDidTap(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension EAMapSearchResultsVC: EAMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var view: EAAnnotationView?
        if let annotation = annotation as? EAMapAnnotation {
            let identifier = annotation.type
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier.rawValue) as? EAAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = EAAnnotationView(annotation: annotation, reuseIdentifier: identifier.rawValue, location: annotation.location)
            }
        }
        return view
    }
    
    func getParentNavigationController() -> UINavigationController? {
        return self.navigationController
    }
    
    func didUpdateLocation(to location: CLLocation) {
        
    }
}
