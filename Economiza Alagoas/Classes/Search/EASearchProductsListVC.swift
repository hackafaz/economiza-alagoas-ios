//
//  EASearchProductsListVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 19/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MBProgressHUD
import IlhasoftCore

class EASearchProductsListVC: UIViewController {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btExit: UIButton!
    @IBOutlet var lbTitle: UILabel!
    
    fileprivate var products: [EAProduct]!
    
    init(_ products: [EAProduct]) {
        super.init(nibName: "EASearchProductsListVC", bundle: nil)
        self.products = products
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        btExit.roundCorners(btExit.frame.width/2)
        lbTitle.isHidden = products.count == 0
    }
    
    fileprivate func setupCollectionView() {
        collectionView.register(UINib(nibName: EAProductSearchResultCVC.reuseId, bundle: Bundle(for: EAProductSearchResultCVC.self)), forCellWithReuseIdentifier: EAProductSearchResultCVC.reuseId)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    @IBAction func btExitDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension EASearchProductsListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let number = products.count
        collectionView.backgroundView = products.count > 0 ? nil : EAEmptyListView(frame: collectionView.frame)
        return number
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EAProductSearchResultCVC.reuseId, for: indexPath) as! EAProductSearchResultCVC
        cell.setup(with: products[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        //TODO: search
        let barCode = products[indexPath.row].barCode
            EASefazAPI.shared.getProducts(byBarCode: barCode, days: 3, coordinates: EALocation.available, radius: 3) {
                result in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let _ = result?["product"] as? EAProduct, let locations = result?["locations"] as? [EALocation] {
                    let resultsVC = EAMapSearchResultsVC(locations, pricesPerCNPJ: result?["pricePerCNPJ"] as? [String: Double])
                    self.navigationController?.pushViewController(resultsVC, animated: true)
                } else {
                    ISAlertMessages.displayMessage("Parece que ocorreu algo de errado. Vamos tentar novamente?", withTitle: "Hum....", fromController: self)
                }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width-10, height: 80)
    }
}
