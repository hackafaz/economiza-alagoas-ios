//
//  EAEmptyListView.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit

class EAEmptyListView: UIView {
    @IBOutlet var contentView:UIView!
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    private func initSubviews() {
        let nib = UINib(nibName: "EAEmptyListView", bundle: Bundle(for: EAEmptyListView.self))
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    
}
