//
//  EASearch.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 14/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import RealmSwift

enum EASortBy {
    case date
    case name
    case price
}

enum EASearchBy: String {
    case category = "category"
    case product = "product"
    case price = "price"
    case location = "location"
}

class EASearch: Object {
    dynamic var searchId: String?
    // The type of object to search for
    dynamic var object: String?
    /// The object id to search for 
    dynamic var objectId: String?
    // The maximum price to search for
    let price = RealmOptional<Double>()
    /// The geographical radius to consider
    let radius = RealmOptional<Int>()
    /// The last time this type of search was realized
    dynamic var lastDate: Date?
}
