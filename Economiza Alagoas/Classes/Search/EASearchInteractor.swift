//
//  EASearchInteractor.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 13/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation

class EASearchInteractor {
    static let shared = EASearchInteractor()
    
    private init() {}
    
    //MARK: General
    func search(_ search: EASearchBy, name: String, sortBy: EASortBy, completion: ((_ categories: [Any])->())) {
        //TODO:
        completion([EAProduct]())
    }
    
    func search(_ search: EASearchBy, code: String, sortBy: EASortBy, completion: ((_ categories: [Any])->())) {
        //TODO:
        completion([EAProduct]())
    }
    
    //MARK: Products
    func getMostSearchedProducts(completion: ((_ categories: [EAProduct])->())) {
        //TODO:
        completion([EAProduct]())
    }
    
//    //MARK: Categories
//    func getMostSearchedCategories(completion: ((_ categories: [EACategory])->())) {
//        //TODO:
//        completion([EACategory]())
//    }
}
