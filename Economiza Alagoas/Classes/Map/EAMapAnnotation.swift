//
//  EAMapAnnotation.swift
//  Economiza Alagoas
//
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import Foundation
import MapKit

enum EAMapAnnotationType: String {
    case regular = "Regular Map View"
    case bestPrice = "Best Price Map View"
}

class EAMapAnnotation: NSObject, MKAnnotation {
    var location: EALocation
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var type:EAMapAnnotationType
    
    init(location: EALocation, type: EAMapAnnotationType) {
        self.location = location
        self.title = location.name
        self.type = type
        self.coordinate = location.coordinates?.coordinate ?? CLLocationCoordinate2D()
    }
    
    var subtitle: String? {
        //TODO
        return "Subtitle"
    }
}
