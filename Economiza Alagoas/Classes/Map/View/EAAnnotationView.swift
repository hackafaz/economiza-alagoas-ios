//
//  EAAnnotationView.swift
//  Economiza Alagoas
//
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit

class EAAnnotationView: MKAnnotationView {
    var type:EAMapAnnotationType!
    var location: EALocation?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(annotation: MKAnnotation?, reuseIdentifier: String?, location: EALocation) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        let vacancyAnnotation = self.annotation as! EAMapAnnotation
        self.type = vacancyAnnotation.type
        self.location = location
        
        switch (vacancyAnnotation.type) {
        case .bestPrice:
            image = #imageLiteral(resourceName: "locationBestPrice")
        case .regular:
            image = #imageLiteral(resourceName: "location")
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if (hitView != nil)
        {
            self.superview?.bringSubview(toFront: self)
        }
        return hitView
    }
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds;
        var isInside: Bool = rect.contains(point);
        if(!isInside)
        {
            for view in self.subviews
            {
                isInside = view.frame.contains(point);
                if isInside
                {
                    break;
                }
            }
        }
        return isInside;
    }
}
