//
//  EAProductInfoCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit


class EAProductInfoCVC: UICollectionViewCell {
    static let reuseId: String = "EAProductInfoCVC"
   
    @IBOutlet var gradientView: GradientView!
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbPrice: UILabel!
    @IBOutlet var lbTime: NSLayoutConstraint!
    @IBOutlet var bestPriceView: UIImageView!
    
    fileprivate var product: EAProduct!

    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        gradientView.roundCorners(10)
//        contentView.addShadow(intensity: .medium)
    }
    
    func setup(with product: EAProduct, price: Double?, isBestPrice: Bool) {
        self.product = product
        lbPrice.isHidden = price == nil
        lbPrice.text = price?.price
        lbName.text = product.name?.localizedUppercase
        lbPrice.text = price?.price
        bestPriceView.isHidden = isBestPrice
    }
}
