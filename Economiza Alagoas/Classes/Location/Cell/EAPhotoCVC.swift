//
//  EAPhotoCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit


class EAPhotoCVC: UICollectionViewCell {
    static let reuseId: String = "EAPhotoCVC"
   
    @IBOutlet weak var imageView: UIImageView!
    
    var items = [Any]()

    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.roundCorners(15)
    }
    
    func setup(with image: UIImage) {
        imageView.image = image
    }
}
