//
//  EAPhotoCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit


class EAMoreItemsCVC: UICollectionViewCell {
    static let reuseId: String = "EAMoreItemsCVC"
   
    @IBOutlet weak var imageView: UIImageView!
    
    var items = [Any]()

    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
