//
//  EALocationInfoCVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit


class EALocationInfoCVC: UICollectionViewCell {
    static let reuseId: String = "EALocationInfoCVC"
   
    @IBOutlet var gradientView: GradientView!
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbAddress: UILabel!
    @IBOutlet var lbTime: NSLayoutConstraint!
    @IBOutlet var lbPrice: UILabel!
    @IBOutlet var lbNumber: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    var location: EALocation!

    //MARK: Init
    override func awakeFromNib() {
        super.awakeFromNib()
        gradientView.roundCorners(10)
//        contentView.addShadow(intensity: .medium)
    }
    
    func setup(with location: EALocation, price: Double?) {
        self.location = location
//        if let image = location.images?[0] {
//            imageView.image = image
//        } else {
//            //TODO:
//        }
        lbName.text = location.name?.localizedUppercase
        lbPrice.text = price?.price
        lbPrice.isHidden = price == nil
        lbNumber.text = location.phone
        lbAddress.text = location.address
    }
}
