//
//  EALocationVC.swift
//  Economiza Alagoas
//
//  Created by Yves Bastos on 12/08/17.
//  Copyright © 2017 Yves Bastos. All rights reserved.
//

import UIKit
import MapKit
import IlhasoftCore
import RAMReel
import CollieGallery

class EALocationVC: GAITrackedViewController {
    @IBOutlet var lbName: UILabel!
    @IBOutlet var btStar: UIButton!
    
    @IBOutlet var cvImages: UICollectionView!
    @IBOutlet var cvMyProducts: UICollectionView!
    @IBOutlet var lbDistance: UILabel!
    @IBOutlet var btIsOpen: UIButton!
    @IBOutlet var lbAddress: UILabel!
    @IBOutlet var lbPhone: UILabel!
    @IBOutlet var btDirections: EAButton!
    
    @IBOutlet var btExit: UIButton!
    @IBOutlet var cvImagesWidthConstraint: NSLayoutConstraint!
    
    fileprivate var images = [UIImage]()
    fileprivate var products = [EAProduct]()
    fileprivate var pricesPerCNPJ = [String: Double]()
    
    fileprivate var location: EALocation!
    
    fileprivate var currentImageIndex: Int?
    //MARK: Init
    init(_ location: EALocation) {
        super.init(nibName: "EALocationVC", bundle: nil)
        self.location = location
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = EAScreens.local.rawValue
        setupLayout()
        setupCollectionViews()
        loadLocationImages()
        setupInfos()
        loadMyFavoriteProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
    
    fileprivate func setupInfos() {
        lbName.text = location.name?.localizedUppercase
        if let address = location.address {
            lbAddress.text = address
        } else {
            lbAddress.text = "Endereço não disponível"
        }
        
        //        btStar.isSelected =
        
        lbPhone.text = location.phone ?? "Sem telefone"
        let coordinates = location.coordinates ?? EALocation.defaultLocation
        let coordinatesLocation = CLLocation(latitude: coordinates.coordinate.latitude, longitude: coordinates.coordinate.longitude)
        if let userLocation = EANavigation.shared.main.mapView.userLocation {
            lbDistance.text = String("\(String(format: "%.2f", coordinatesLocation.distance(from: userLocation)/1000)) km distante")
        } else {
            lbDistance.text = "Ative seu GPS para saber a distância"
        }
        self.btIsOpen.isHidden = true
//        let open = Bool.random()
//        let openText = open ? "Aberto" : "Fechado"
//        self.btIsOpen.setTitle(openText, for: .normal)
//        self.btIsOpen.backgroundColor = open ? UIColor(rgba: "#EAFAFF") : UIColor(rgba: "#FFEAEA")
        let isBtSelected = EAUser.current().getLocations().contains(where: {$0.cnpj == location.cnpj})
        btStar.setImage(isBtSelected ? #imageLiteral(resourceName: "star") : #imageLiteral(resourceName: "starOff"), for: .normal)
    }
    
    fileprivate func setupLayout() {
        btIsOpen.isHidden = true
        cvImagesWidthConstraint.constant = view.frame.width
        btExit.roundCorners(btExit.frame.width/2)
        btDirections.roundCorners(btDirections.frame.width/2)
        btIsOpen.roundCorners(7.5)
    }
    
    fileprivate func setupCollectionViews() {
        cvMyProducts.register(UINib(nibName: EAProductInfoCVC.reuseId, bundle: Bundle(for: EAProductInfoCVC.self)), forCellWithReuseIdentifier: EAProductInfoCVC.reuseId)
        
        cvImages.delegate = self
        cvImages.dataSource = self
        cvMyProducts.dataSource = self
        cvMyProducts.delegate = self
        cvImages.reloadData()
        cvMyProducts.reloadData()
    }
    
    fileprivate func loadLocationImages() {
        //        guard let location2d = location.location else {
        //            return
        //        }
        
        EAPlacesInteractor.shared.getPhotos(from: location) {
            photos, error in
            if let photos = photos {
                self.images = photos
                self.cvImages.reloadData()
            } else {
                //TODO:
            }
        }
    }
    
    fileprivate func loadMyFavoriteProducts() {
        if let cnpj = location.cnpj {
            EASefazAPI.shared.getFavoriteProductsInLocation(cnpj: cnpj) {
                productPrices in
                
                if let productPrices = self.products as? [EAProduct: Double] {
                    self.products = productPrices.map({$0.key})
                }
                
                self.cvMyProducts.reloadData()
            }
        } else {
            ISAlertMessages.displayErrorMessage("CNPJ não encontrado", fromController: self)
        }
    }
    
    fileprivate func setupCollieGallery() {
        
    }
    
    //MARK: Touch
    @IBAction func btDirectionsDidTap(_ sender: Any) {
        if let coordinates = location.coordinates?.coordinate {
            EANavigation.shared.openInMaps(name: location.name, coordinates: coordinates)
        } else {
            ISAlertMessages.displayMessage("Não encontramos as coordenadas... te encontramos no Google Maps?", withTitle: "Hum...", fromController: self)
        }
    }
    
    @IBAction func btStarDidTap(_ sender: UIButton) {
        btStar.isSelected = !btStar.isSelected
        btStar.setImage(btStar.isSelected ? #imageLiteral(resourceName: "star") : #imageLiteral(resourceName: "starOff"), for: .normal)
        
        btStar.isSelected ? EAUser.current().saveFavorite(location: location, completion: {_ in }) : EAUser.current().removeFavorite(location: location, completion: {_ in})
    }
    
    @IBAction func btExitDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func btSearchDidTap() {
        
    }
}

extension EALocationVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let number = collectionView == cvImages ? images.count : products.count
        return number
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        if collectionView == cvImages {
            cell = cvImages.dequeueReusableCell(withReuseIdentifier: EAPhotoCVC.reuseId, for: indexPath)
            (cell as! EAPhotoCVC).setup(with: images[indexPath.row])
        } else if collectionView == cvMyProducts {
            cell = cvMyProducts.dequeueReusableCell(withReuseIdentifier: EAProductInfoCVC.reuseId, for: indexPath)
            let cnpj = location.cnpj
            let price = cnpj != nil ? pricesPerCNPJ[cnpj!] : nil
            (cell as! EAProductInfoCVC).setup(with: products[indexPath.row], price: price, isBestPrice: indexPath.row == 0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvImages {
            currentImageIndex = indexPath.row
            let galleryView = cvImages.cellForItem(at: indexPath)!.contentView
            let options = EAImageUtil.getDefaultCollieGalleryOptions()
            options.showCloseButton = false
            options.openAtIndex = indexPath.row
            let collieImages = self.images.map({CollieGalleryPicture(image: $0)})
            let gallery = CollieGallery(pictures: collieImages, options: options, theme: nil, initialCustomSize: nil)
            gallery.delegate = self
            gallery.presentInViewController(self, transitionType: CollieGalleryTransitionType.zoom(fromView: galleryView, zoomTransitionDelegate: self), completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize!
        
        if collectionView == cvImages {
            size = CGSize(width: 270, height: 150)
        } else {
            size = CGSize(width: 150, height: 80)
        }
        return size
    }
}

extension EALocationVC: CollieGalleryDelegate, CollieGalleryZoomTransitionDelegate {
    func gallery(_ gallery: CollieGallery, indexChangedTo index: Int) {
        currentImageIndex = index
    }
    
    func zoomTransitionContainerBounds() -> CGRect {
        return view.bounds
    }
    
    func zoomTransitionViewToDismissForIndex(_ index: Int) -> UIView? {
        var view: UIView?
        if let row = currentImageIndex {
            view = cvImages.cellForItem(at: IndexPath(row: row, section: 0))
        }
        return view
    }
}
