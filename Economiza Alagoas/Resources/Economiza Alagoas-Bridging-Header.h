//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//


#ifndef Economiza_Alagoas_Bridging_Header_h
#define Economiza_Alagoas_Bridging_Header_h

#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAILogger.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAITrackedViewController.h>
#import <GoogleAnalytics/GAITracker.h>

#endif /* Economiza_Alagoas_Bridging_Header_h */
